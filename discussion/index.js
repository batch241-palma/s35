// Use the "require" directive to load the express module/package
const express = require("express");
// Allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulation our database
const mongoose = require("mongoose");

// Create an application using express
const app = express(); 

// For our application server to run, we need a port to listen to
const port = 3000;


// [SECTION] MongoDB connection
/*
mongoose.connect("<MongoDB connection string>, {useNewUrlParser : true}")
*/
// Connecting to MongoDB atlas
// Add password and database name
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.z41cdv5.mongodb.net/s35?retryWrites=true&w=majority", {
	// Due to update in MongoDB drivers that allow connection to it, the default connection is being flagged as error
	// allows us to avoid any current and future errors while connecting to MongoDB
	useNewUrlParser : true,
	useUnifiedTopology: true
});

// Connecting to MongoDB locally
let db = mongoose.connection;
// if a connection error occured, print the output in terminal
db.on("error", console.error.bind(console, "connection error"));
// if the connection is successful, print the message "Connected to MongoDB Atlas!"
db.once("open", () => console.log("Connected to MongoDB Atlas!"));

// Methods used from express JS are middlewares
// Middleware is software that provides common services and capabilities to applications outside of what’s offered by the operating system

// Allows your app to read json data
app.use(express.json());

// Allows your app to read data from forms
// {extended:true} allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// [SECTION] Mongoose Schema

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// [SECTION] Models
// Models must be in singular form and capitalized
// first parameter - indicates the collection in where to store data
// second parameter - specify the schema/bluprint of the documents that will be stored in the MongoDB colection
const Task = mongoose.model("Task", taskSchema);


// Creating a new Task
/*
Business Logic
 Add a functionality to check if there are duplicate tasks
- If the task already exists in the database, we return an error
- If the task doesn't exist in the database, we add it in the database
The task data will be coming from the request's body
Create a new Task object with a "name" field/property
The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) => {
	// To check if there are duplicate tasks
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found!");
		} else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask) => {
				if(saveErr){
				return console.error(saveErr);
			} else {
				return res.status(201).send("New task created");
			};

			})
		};
	});
});

// Get all the Tasks
/*
Business Logic
1. Retrieve all documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			});
		};
	});
});



// Tells our server to listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));
