/* Activity S35 */

const express = require("express");

const mongoose = require("mongoose");

const app = express(); 

const port = 3000;


mongoose.connect("mongodb+srv://admin123:admin123@cluster0.z41cdv5.mongodb.net/s35?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas!"));

app.use(express.json());

app.use(express.urlencoded({extended:true}));

/* No.1 */
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

/* No.2 */
const User = mongoose.model("User", userSchema);

/* No.3 */
app.post("/signup", (req, res) => {
	
	User.createIndexes({
		username: req.body.username, 
		password: req.body.password}, (err, result) => {
		if(req.body.username !== '' && req.body.password !== ''){
       		let newUser = new User({
        	username: req.body.username,
			password: req.body.password
			});

        	newUser.save((saveErr, savedUser) => {
			if(saveErr){
				return console.error(saveErr);
			} else {
				return res.status(201).send("New User Registered");
			};

			})
    	} else {
    		return res.send("Register one")
    	}

	});
});

app.get("/signup", (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				Users: result
			});
		};
	});
});



// Tells our server to listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));
